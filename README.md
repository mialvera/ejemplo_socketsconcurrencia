# README #

Ejemplos que usan la interface sockets en Linux, basados en los ejemplos del capítulo de libro [Computer Systems: A Programmer's Perspective, 3/E](http://csapp.cs.cmu.edu/3e/home.html)

### Compilación ###

* Compilar el cliente: 
	* make client

* Compilar el servidor:
	* make server

* Compilar todo:
	* make
	
### Consideración en execve vs execvp ###

#### execvp ####

Funciona de manera similar a execve, sin embargo, no recibe las variables de entorno, porque trabaja con los valores definidos (directorios) en la variable de entorno PATH.

Por ejemplo:
	
```
char *args[] = {"ls", "-la"};

args[0] = "ls";
 
execvp(args[0], args);
```

#### execve ####

En cambio, aquí se necesita especificar el ambiente para el proceso, utilizando el argumento env.

Sin embargo, al momento de ejecutar el comando, se debe escribir con la ruta especificada. Por ejemplo para ejecutar ls, se debe escribir /bin/ls en el programa para que funcione corrrectamente.


Por ejemplo:
	
```
char *envp[] = {"PATH=/bin", (char *)0};

char *args[] = {"/bin/ls", "-la"};

args[0] = "/bin/ls";
 
execve(args[0], args, envp);
```

### Uso ###
Ejecutar el cliente:

```
./client <host> <port>
```
Ejemplo:

```
./client 127.0.0.1 8080
```

Ejecutar el cliente:

```
./server <port>
```
Ejemplo:

```
./server 8080
```