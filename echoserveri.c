#include "csapp.h"
 #include <unistd.h>

void echo(int connfd);
void sigchld_handler(int sig);
void setup(char inputBuffer[], char *args[]);

int main(int argc, char **argv)
{

	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	Signal(SIGCHLD, sigchld_handler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);


		if(Fork() == 0){

			close(listenfd);
			echo(connfd);
			Close(connfd);
			exit(0);
		}
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	char *envp[] =
    {
        "PATH=/bin:/usr/bin",
        (char *)0
    };
	size_t n;
	char buf[MAXLINE];
	rio_t rio;


	Signal(SIGCHLD, sigchld_handler);
	

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {


		char *args[100];

		pid_t pid;

		if((pid = Fork()) == 0){
			
			buf[n - 1] = '\0';
			char *copiaBuf = malloc(strlen(buf));
			
			strcpy(copiaBuf, buf);

			setup(copiaBuf, args);

			printf("\nComando proveniente del cliente: %s\n", buf);

			// Funciona de manera similar a execve, sin embargo, no recibe
			// las variables de entorno.
			// Por ejemplo:
			// args[0] = "ls"
			// args[] = {"ls", "-la"}
			// execvp(args[0], args);


			// A diferencia de execvp, el trato que tiene es diferente, y se
			// procede de la siguiente manera:
			// Al ingresar el comando, se lo debe hacer con su respectiva
			// ubicacion
			// Si el comando es ls -> debe ingresarse /bin/ls
			// Descomentar siguiente linea si desea probar funcionalidad
			// de execve
			execve(args[0], args, envp);

		}
		char mensaje[MAXLINE];

		int status;
		if (waitpid(pid, &status, 0) < 0) {
			strcpy(mensaje, "ERROR");
		}
		else {
			strcpy(mensaje, "OK");
		}

		Write(connfd, mensaje , strlen(mensaje) + 1);

		
	}
}

void sigchld_handler(int sig){

	while(waitpid(-1, 0, WNOHANG) >0)
		;
	return;
}

void setup(char inputBuffer[], char *args[]) 
{
    const char s[1] = " ";
    char *token;
    token = strtok(inputBuffer, s);
    int i = 0;
    while( token != NULL)
    {
        args[i] = token;
        i++;
        token = strtok(NULL,s);
    }
    args[i] = NULL;
}